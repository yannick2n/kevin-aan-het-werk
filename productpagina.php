<!DOCTYPE html>
<html>
<head>
    <?php
    //Start session
    session_start();
    // The connection to the database
    include 'connect.php';
    $hsql = "SELECT DISTINCT sg.stockgroupname, sg.StockGroupID
            FROM StockItems s
            JOIN StockItemStockGroups g
            ON s.stockitemid = g.stockitemid
            JOIN StockGroups sg
            ON g.stockgroupid = sg.StockGroupID
            ORDER BY sg.StockGroupID";
    $hresult = $conn->query($hsql);
    if (!$hresult->num_rows > 0) {
        echo "0 results";
        return;
    }
    $conn->close();
    ?>
    <head>
        <!--===============================================================================================-->
        <link rel="icon" type="image/png" href="images/icons/favicon.png"/>
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="fonts/themify/themify-icons.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="fonts/elegant-font/html-css/style.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/slick/slick.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/lightbox2/css/lightbox.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="css/util.css">
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <link rel="stylesheet" type="text/css" href="css/styleproduct.css">
        <link rel="stylesheet" type="text/css" href="css/winkelwagen.css">
        <!--===============================================================================================-->
    </head>
    <div class="header1">
        <!-- Header desktop -->
        <div class="wrap_header">
            <!-- Logo -->
            <a href="index.php" class="logo">
                <img src="images/wwi.png" alt="IMG-LOGO">
            </a>

            <div class="wrap_menu">
                <nav class="menu">
                    <ul class="main_menu">
                        <li class="nav-item">
                            <div class="dropdown">
                                <a class="nav-link dropbtn">Producten</a>
                                <div class="dropdown-content">
                                    <?php
                                    while ($row = $hresult->fetch_assoc()) {
                                        echo '<p><a href = "categorie.php?id=' . $row["StockGroupID"] . '">' . $row["stockgroupname"] . '</a></p>';
                                    }
                                    ?>
                                </div>
                            </div>
                        </li>

                        <li>
                            <a href="about.php">Over ons</a>
                        </li>

                        <li>
                            <a href="contact.php">Contact</a>
                        </li>
                    </ul>
                </nav>
            </div>

            <!-- Header Icon --

                        <!-- Header cart noti -->
            <div class="header-icons">
                <form method="post" action="zoeken.php">
                    <div class="row rijzoeken">
                        <div class="balk">
                            <input type="text" name="zoeken" class="zoekbalk" placeholder="<?php if($_SESSION['status'] == 1){
                                echo ("Hallo ". $_SESSION['LogonName']. ",Wat zoek je");
                            } else {
                                echo ("Wat zoek je?");
                            }
                            ?>">
                        </div>
                        <div class="knop">
                            <input type="submit" name="search" value="zoeken" class="zoekknop">
                        </div>
                    </div>
                </form>

                <span class="linedivide1"></span>

                <?php
                if ($_SESSION['status'] == 1) {
                    echo "<div class='logonname'>Hallo, ". "<strong class='kleur'>". $_SESSION['LogonName']. "</strong></div>";
                } else {
                    echo "<div class='logonname'><a class='inlogbtn' href='inlogscherm.php'>Login</a></div>";
                }
                ?>
                <a href="inlogscherm.php" class="header-wrapicon1 dis-block">
                    <img src="images/icons/icon-header-01.png" class="header-icon1" alt="ICON">
                </a>

                <span class="linedivide1"></span>

                <div class="header-wrapicon2">
                    <a href="Winkelwagen.php"><img src="images/icons/icon-header-02.png" class="header-icon1" alt="ICON"></a>
                    <span class="header-icons-noti"><?php echo $_SESSION['waardes']; ?></span>
                </div>
            </div>
        </div>
    </div>

<body>

<!-- Begin van header gedeelte -->

<!-- Einde van header gedeelte -->

<br><br>
<div class="wrapper">
    <div class="product_left">

        <!-- Slideshow container -->
        <div class="slideshow-container">
            <?php
            include 'connect.php';
            $post = htmlspecialchars($_GET["id"]);

            $sql1 = "SELECT StockitemName, s.Photo, recommendedretailprice, sg.stockgroupname, marketingcomments
            FROM StockItems s 
            JOIN StockItemStockGroups g 
            ON s.stockitemid = g.stockitemid 
            JOIN StockGroups sg 
            ON g.stockgroupid = sg.StockGroupID
            WHERE s.StockitemID = " . $post;
            $result1 = $conn->query($sql1);
            if ($result1->num_rows > 0) {
                $category = array();
                while ($row = $result1->fetch_assoc()) {
                    $name = $row["StockitemName"];
                    $price = $row["recommendedretailprice"];
                    $category[] = $row["stockgroupname"];
                    $comment = $row["marketingcomments"];
                    $photo = $row["Photo"];
                    $photo2 = $row["Photo2"];
                }
            } else {
            }
            $conn->close();
            ?>
            <!-- Full-width images with number and caption text -->
            <div class="mySlides fade">
                <div class="numbertext">1 / 2</div>
                <?php
                echo " <img src='data:image/jpeg;base64," . base64_encode($photo) . "' style=width:100%>";
                ?>
            </div>

            <div class="mySlides fade">
                <div class="numbertext">2 / 2</div>
                <?php
                echo " <img src='data:image/jpeg;base64," . base64_encode($photo2) . "'>";
                ?>
            </div>

            <!-- Next and previous buttons -->
            <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
            <a class="next" onclick="plusSlides(1)">&#10095;</a>
        </div>
        <br>

        <!-- The dots/circles -->
        <div style="text-align:center">
            <span class="dot" onclick="currentSlide(1)"></span>
            <span class="dot" onclick="currentSlide(2)"></span>
        </div>


        </div>
        <div class="product_right">
            <h2><?php echo $name; ?></h2>

            <h3><b>€<?php echo $price; ?> </b><i> Excl. Verzendkosten</i></h3>

            <p><?php echo $comment ?></p>
            <a href="verwerk.php?id=<?php echo $post; ?>&action=add"><input class="buttonclass" type="submit" value="In winkelwagen" name="addtocart" /></a><br><br>
            <b>Categorieën: </b>
            <?php
            $i = sizeof($category);
            foreach ($category as $cat) {
                $i--;
                if ($i != 0) {
                    echo "$cat, ";
                    } else {
                    echo $cat;
                    }
                    }
                    ?>
        </div>
</div>

<script>

    var slideIndex = 1;
    showSlides(slideIndex);


    function plusSlides(n) {
        showSlides(slideIndex += n);
    }

    function currentSlide(n) {
        showSlides(slideIndex = n);
    }

    function showSlides(n) {
        var i;
        var slides = document.getElementsByClassName("mySlides");
        var dots = document.getElementsByClassName("dot");
        if (n > slides.length) {slideIndex = 1}
        if (n < 1) {slideIndex = slides.length}
        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";
        }
        for (i = 0; i < dots.length; i++) {
            dots[i].className = dots[i].className.replace(" active", "");
        }
        slides[slideIndex-1].style.display = "block";
        dots[slideIndex-1].className += " active";
    }
</script>                                                                                                                                                                                                                                                                                                          </body>
                                                                                                                                                                                                                                                                                                                            </html>

