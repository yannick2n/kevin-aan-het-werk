
<?php
// begin de sessie
//session_start();
include 'header.php'
?>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Importeer alle CSS bestanden -->
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="images/icons/favicon.png"/>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/themify/themify-icons.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/elegant-font/html-css/style.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/slick/slick.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/lightbox2/css/lightbox.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="css/util.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="css/styleproduct.css">
    <link rel="stylesheet" type="text/css" href="css/winkelwagen.css">
    <!--===============================================================================================-->

</head>
<body>
    <section class="bg-title-pagezoeken p-t-50 p-b-900 flex-col-c-m" style="background-image: url(images/bannerwwi.png);">
        <h2 class="l-text2 t-center">
            <br>
        </h2>
        <p class="m-text13 t-center">
            <br>
        </p>
    </section>


    <?php
//definieer het ip adres van de server, de username, het wachtwoord en de te gebruiken database
//include ("db.php");
    $servername = "127.0.0.1";
    $username = "root";
// wachtwoord nog aanpassen!
    $password = "";
    $dbname = "wideworldimporters";
// leg de verbinding vast
    $conn = new PDO('mysql:host=localhost;dbname=wideworldimporters', $username, $password);
//als er geen verbinding kan worden gemaakt geef dan een error terug
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    //neem de input van de zoekbalk over, en zet het om naar html-karakters
    $zoek = filter_input(INPUT_POST, 'zoeken');
    $zoek = htmlspecialchars($zoek);
    //Probeer om de de query $sql uit te voeren, maakt gebruik van $zoek
    try {
        $sql = "SELECT photo, stockitemname, recommendedretailprice, stockitemid FROM stockitems WHERE stockitemname LIKE '%$zoek%' OR tags LIKE '%$zoek%'";
        $zoek_array = $conn->prepare($sql);
        $zoek_array->execute();
    }
    //mits dit niet lukt geef dan een error terug
    catch (PDOException $e) {
        print($e->getMessage());
    }
    // loop langs alle rijen met de uitkomst van query $zoek_array
    print(" Zoekresultaten voor '" . $zoek . "'");
    //Voor elke rije, print dan de volgende html code
    while ($row = $zoek_array->fetch()) {
        ?>
        <div class="col-sm-12 col-md-6 col-lg-4 p-b-50">
            <!-- Block2 -->
            <div class="block2">
                <div class="block2-img wrap-pic-w of-hidden pos-relative">
                    <img src='data:image/jpeg;base64," .<?php base64_encode($row['Photo']) ?>. "'>"

                    <div class="block2-overlay ">
                        <a href="#" class="block2-btn-addwishlist hov-pointer trans-0-4">
                            <i class="icon-wishlist icon_heart_alt" aria-hidden="true"></i>
                            <i class="icon-wishlist icon_heart dis-none" aria-hidden="true"></i>
                        </a>

                        <div class="block2-btn-addcart w-size1 trans-0-4">
                            <!-- Button -->
                            <form method="post" action="categorie.php?action=add&code=<?php echo $row["stockitemid"]; ?>">
                                <div class="cart-action"><input type="text" class="product-quantity" name="quantity" value="1" size="2" /><input type="submit" value="Winkelwagen" class="btnAddAction" /></div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="block2-txt p-t-20">
                    <a href="productpagina.php?action=add&id=' .<?php $row["stockitemid"];?> . '" class="block2-name dis-block s-text3 p-b-5">
                        <?php echo $row["stockitemname"]; ?>
                    </a>

                    <span class="block2-price m-text6 p-r-5">
                        <?php echo "€" . $row["recommendedretailprice"]; ?>
                    </span>
                </div>
            </div>
        </div>
        <?php
    }
    ?>
    <div class="t-center s-text8 p-t-20">
        Copyright � 2018 All rights reserved. | Deze webshop is gemaakt met <i class="fa fa-heart-o" aria-hidden="true"></i> door de ICT heroes van groep 3.
    </div>



    <!-- Back to top -->
    <div class="btn-back-to-top bg0-hov" id="myBtn">
        <span class="symbol-btn-back-to-top">
            <i class="fa fa-angle-double-up" aria-hidden="true"></i>
        </span>
    </div>

    <!-- Container Selection1 -->
    <div id="dropDownSelect1"></div>



    <!--===============================================================================================-->
    <script type="text/javascript" src="vendor/jquery/jquery-3.2.1.min.js"></script>
    <!--===============================================================================================-->
    <script type="text/javascript" src="vendor/animsition/js/animsition.min.js"></script>
    <!--===============================================================================================-->
    <script type="text/javascript" src="vendor/bootstrap/js/popper.js"></script>
    <script type="text/javascript" src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <!--===============================================================================================-->
    <script type="text/javascript" src="vendor/select2/select2.min.js"></script>
    <script type="text/javascript">
        $(".selection-1").select2({
            minimumResultsForSearch: 20,
            dropdownParent: $('#dropDownSelect1')
        });
    </script>
    <!--===============================================================================================-->
    <script type="text/javascript" src="vendor/slick/slick.min.js"></script>
    <script type="text/javascript" src="js/slick-custom.js"></script>
    <!--===============================================================================================-->
    <script type="text/javascript" src="vendor/countdowntime/countdowntime.js"></script>
    <!--===============================================================================================-->
    <script type="text/javascript" src="vendor/lightbox2/js/lightbox.min.js"></script>
    <!--===============================================================================================-->
    <script type="text/javascript" src="vendor/sweetalert/sweetalert.min.js"></script>
    <script type="text/javascript">
        $('.block2-btn-addcart').each(function () {
            var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
            $(this).on('click', function () {
                swal(nameProduct, "is added to cart !", "success");
            });
        });

        $('.block2-btn-addwishlist').each(function () {
            var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
            $(this).on('click', function () {
                swal(nameProduct, "is added to wishlist !", "success");
            });
        });
    </script>

    <!--===============================================================================================-->
    <script src="js/main.js"></script>

</body>