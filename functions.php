<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function controleermail($email) {
    $user = "root";
    $pass = "";
    $dbh = new PDO('mysql:host=127.0.0.1;dbname=wideworldimporters', $user, $pass);

    $sth = $dbh->prepare("SELECT * FROM klant WHERE Email = :Emailad");
    $sth->bindValue(':Emailad', $email, PDO::PARAM_STR);

    $sth->execute();

    $sth->execute();
    $aantal1 = $sth->rowCount();

    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        echo "<br><br>Onjuist email adres ingevuld";
        return FALSE;
    } elseif ($aantal1 != 0) {
        echo "<br><br>Het email adres is al ingebruik.";
        return FALSE;
    } else {
        return TRUE;
    }
}

function controleergebruiker($gebruiker) {
    $user = "root";
    $pass = "";
    $dbh = new PDO('mysql:host=127.0.0.1;dbname=wideworldimporters', $user, $pass);

    $sth = $dbh->prepare("SELECT * FROM klant WHERE LogonName = :LogonName");
    $sth->bindValue(':LogonName', $gebruiker, PDO::PARAM_STR);

    $sth->execute();

    $sth->execute();
    $aantal2 = $sth->rowCount();

    if ($aantal2 != 0) {
        echo "<br><br>Gebruikersnaam is bezet.";
        return FALSE;
    } else {
        return TRUE;
    }
}

function controleervelden($naam, $telefoonnummer, $adres, $postcode, $woonplaats, $Emailad, $LogonName, $wachtwoord1, $wachtwoord2) {
    if (preg_match("#[a-z]+#", $telefoonnummer) OR strlen($telefoonnummer) > 20 OR strlen($telefoonnummer) < 8) {
        echo "<br><br>Ongeldig telefoonnummer ingevuld";
        return FALSE;
    } elseif (preg_match("#[0-9]+#", $naam) OR strlen($naam) > 50 OR preg_match('/[^a-z\s-]/i', $naam)) {
        echo "<br><br> Ongeldige naam ingevuld";
        return FALSE;
    } elseif (strlen($adres) > 30 OR strlen($adres) < 8 OR ! preg_match('/[^a-z\s-]/i', $adres)) {
        echo "<br><br>Ongeldig adres ingevuld";
        return FALSE;
    } elseif (strlen($postcode) < 5 OR strlen($postcode) > 8 OR ! preg_match('/[^a-z\s-]/i', $postcode)) {
        echo "<br><br>Ongeldige postcode ingevuld";
        return FALSE;
    } elseif (empty($woonplaats) OR strlen($postcode) > 30) {
        return FALSE;
        echo "<br><br>Woonplaats niet ingevuld";
    } elseif (empty($Emailad)) {
        return FALSE;
        echo "<br><br>Email niet ingevuld";
    } elseif (empty($LogonName)) {
        return FALSE;
        echo "<br><br>Gebuiker niet ingevuld";
    } elseif ($wachtwoord1 != $wachtwoord2 OR empty($wachtwoord1)) {
        echo "<br><br>Ingevulde wachtwoorden komen niet overeen";
        return FALSE;
    } elseif (strlen($wachtwoord1) < 6 OR strlen($wachtwoord1) > 20 OR ! preg_match("#[0-9]+#", $wachtwoord1) OR ! preg_match("#[a-z]+#", $wachtwoord1)) {
        echo"<br><br>Wachtwoord voldoet niet aan de gestelde eisen: minimaal 6 tekens waarvan minimaal 1 cijfer.";
        return FALSE;
    } else {
        return TRUE;
    }
}

function naamplakken($voornaam, $tussenvoegsel, $achternaam) {
    return "$voornaam $tussenvoegsel $achternaam";
}



function userData() {
if (isset($_SESSION['Klantid'])) {
include 'connect.php';
$sql = "SELECT Klantid, naam, telefoonnummer, adres, postcode,woonplaats , logonname, email, hashedpassword, ispermittedtologon FROM Klant WHERE Klantid = " . $_SESSION['Klantid'];
$result = $conn->query($sql);
if ($result->num_rows == NULL) {
return NULL;
} else {
while ($row = $result->fetch_assoc()) {
$array = array('klantid' => $row['klantid'], 'Email' => $row['email'], 'HashedPassword' => $row['hashedpassword'], 'ispermittedtologon' => $row['ispermittedtologon'], 'naam' => $row['naam'], 'adres' => $row['adres'], 'woonplaats' => $row['woonplaats'], 'Phonenumber' => $row['telefoonnummer']);
return $array;
}
}
} else {
return NULL;
}
}
function returnCategories() {
    include 'connect.php';
    $sql = "SELECT stockgroupid, Stockgroupname FROM stockgroups";
    $result = $conn->query($sql);
    while ($row = $result->fetch_assoc()) {
        $array[$row["stockgroupid"]] = $row["Stockgroupname"];
    }
    return $array;
    $conn->close();
}

function returnFromDatabase($par1, $par2, $table) {
    include 'connect.php';
    $array = array();
    $sql = "SELECT $par1, $par2 "
        . "FROM $table";
    $result = $conn->query($sql);
    if (!$result->num_rows > 0) {
        return null;
    } else {
        while ($row = $result->fetch_assoc()) {
            $array[$row["$par1"]] = $row["$par2"];
        }
    }
    return $array;
    $conn->close();
}

function isCurrentUserAdmin() {
    $udata = userData();
    if ($udata['LogonName'] == "Beheerder") {
        return true;
    } else {
        return false;
    }
}