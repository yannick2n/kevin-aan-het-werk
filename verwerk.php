<?php

session_start();
$id = htmlspecialchars($_GET["id"]);
$action = htmlspecialchars($_POST["action"]);

if ($action == "add") {
    function verhoog($id) {
        foreach ($_SESSION as $i => $w) {
            if ($i == 'id' . $id) {
                //verhoog waarde met 1
                $_SESSION['id' . $id] = $w + 1;
                return true;
            } 
        }
        return false;
    }

    if (!verhoog($id)) {
        $_SESSION['id' . $id] = '1';
    }
} else if ($action == "remove") {
    foreach ($_SESSION as $i => $w) {
        echo $w;
        if ($w != 1) {
            $_SESSION['id' . $id] = $w - 1;
        } else {
            unset($_SESSION['id' . $id]);
        }
    }
}

if ($action == "removeall") {
    unset($_SESSION['id' . $id]);
}



include "connect.php";
if ($action == "addItemToDatabase") {
        $name = htmlspecialchars($_POST['name']);
        $category = htmlspecialchars($_POST['category']);
        $tags1 = htmlspecialchars($_POST['tags']);
        $supplier = htmlspecialchars($_POST['supplier']);
        $color = htmlspecialchars($_POST['color']);
        $brand = htmlspecialchars($_POST['brand']);
        $package = htmlspecialchars($_POST['package']);
        $outer = htmlspecialchars($_POST['outer']);
        $size = htmlspecialchars($_POST['size']);
        $timedays = htmlspecialchars($_POST['timedays']);
        $quantity = htmlspecialchars($_POST['quantity']);
        $cstock = htmlspecialchars($_POST['cstock']);
        $bcode = htmlspecialchars($_POST['brand']);
        $taxrate = htmlspecialchars($_POST['package']);
        $uprice = htmlspecialchars($_POST['uprice']);
        $marketing = htmlspecialchars($_POST['marketing']);
        $weight = htmlspecialchars($_POST['weight']);
        $tags = explode(', ', $tags1);
        $tags2 = "[";
        foreach ($tags as $i => $w) {
            if ($i == sizeof($tags) - 1) {
                $tags2 .= '"' . $w . '"';
            } else {
                $tags2 .= '"' . $w . '", ';
            }
        }
        $tags2 .= "]";
        $CustomerID = 1;
        $StockItemsql = "SELECT max(StockItemID) as id FROM StockItems";
        $StockItemIDresult = $conn->query($StockItemsql);
        $StockItemID = "";
        if ($StockItemIDresult->num_rows == NULL) {
            return NULL;
        } else {
            while ($row = $StockItemIDresult->fetch_assoc()) {
                $StockItemID = $row['id'] + 1;
            }
        }
        $sql = "INSERT INTO StockItems"
            . "(StockItemID, StockItemName, SupplierID, ColorID, UnitPackageID, OuterPackageID, Brand, size, leadtimedays, quantityperouter, ischillerstock, barcode, taxrate, unitprice, recommendedretailprice, typicalweightperunit, marketingcomments, tags, searchdetails, lasteditedby, validfrom, validto)"
            . " VALUES"
            . "('$StockItemID', '$name', '$supplier', '$color', '$package', '$outer', '$brand', '$size', '$timedays', '$quantity', '$cstock', '$bcode', '$taxrate', '$uprice', '$uprice', '$weight', '$marketing', '$tags2', '$name', '$CustomerID', '2013-12-31 00:00:00', '9999-31-12 00:00:00')";
        $result = $conn->query($sql);
        $stockgroupid = "SELECT MAX(StockItemStockGroupID)+1 as id FROM stockitemstockgroups";
        $sgid = $conn->query($stockgroupid);
        if ($sgid->num_rows == NULL) {
            return NULL;
        } else {
            while ($row = $sgid->fetch_assoc()) {
                $StockItemGID = $row['id'] + 1;
            }
        }
        $sql = "INSERT INTO stockitemstockgroups VALUES ('$StockItemGID',  '$StockItemID', '$category', '$CustomerID', '2018-11-21 10:59:48')";
        $result2 = $conn->query($sql);
        header("location: beheerder.php");
} else if ($action == "removeItemFromDatabase") {
        $id = htmlspecialchars($_POST['id']);
        $sql = "DELETE FROM stockitemstockgroups WHERE StockItemID = $id";
        $result = $conn->query($sql);
        $sql2 = "DELETE FROM stockitems WHERE StockItemID = $id;";
        $result2 = $conn->query($sql2);
        header("location: beheerder.php");
} else if ($action == "addCategoryToDatabase") {
        $array = userData();
        $CustomerID = 1;
        $name = htmlspecialchars($_POST['name']);
        $sql = "SELECT MAX(StockGroupID)+1 as id FROM stockgroups";
        $result = $conn->query($sql);
        if ($result->num_rows == NULL) {
            return NULL;
        } else {
            while ($row = $result->fetch_assoc()) {
                $StockGroupID = $row['id'];
            }
        }
        $sql2 = "INSERT INTO stockgroups VALUES ($StockGroupID, '$name', '$CustomerID', '2013-01-01 00:00:00', '9999-12-31 23:59:59')";
        $result2 = $conn->query($sql2);
        header("location: beheerder.php");
} else if ($action == "editCategory") {
        $id = htmlspecialchars($_POST['id']);
        $name = htmlspecialchars($_POST['name']);
        $sql = "UPDATE stockgroups SET stockgroupname = '$name' WHERE stockgroupid = $id";
        $result = $conn->query($sql);
        header("location:beheerder.php");
} else if ($action == "deleteCategory") {
        $id = htmlspecialchars($_POST['id']);
        $action = htmlspecialchars($_POST['dothis']);
        if ($action == "delete") {
            $sql1 = "SELECT StockItemID as id FROM Stockitemstockgroups WHERE stockgroupid = $id";
            $result1 = $conn->query($sql1);
            $sql2 = "DELETE FROM stockitemstockgroups WHERE stockgroupid = $id;";
            $result2 = $conn->query($sql2);
            while ($row = $result1->fetch_assoc()) {
                $sql = "DELETE FROM StockItems WHERE stockItemID = " . $row['id'];
                $result = $conn->query($sql);
            }
        } else {
            $sql = "UPDATE stockitemstockgroups SET StockGroupID = $action WHERE StockGroupID = $id";
            $result = $conn->query($sql);
        }
        $sql3 = "DELETE FROM stockgroups WHERE stockgroupid = $id";
        $result3 = $conn->query($sql3);
        header("location:beheerder.php");
}

