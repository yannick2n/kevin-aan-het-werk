<html>
<?php
//Start session
session_start();
// The connection to the database
include 'connect.php';
include 'functions.php';
$hsql = "SELECT DISTINCT sg.stockgroupname, sg.StockGroupID
            FROM StockItems s
            JOIN StockItemStockGroups g
            ON s.stockitemid = g.stockitemid
            JOIN StockGroups sg
            ON g.stockgroupid = sg.StockGroupID
            ORDER BY sg.StockGroupID";
$hresult = $conn->query($hsql);
if (!$hresult->num_rows > 0) {
    echo "0 results";
    return;
}
$conn->close();
?>
<head>
    <title>WWI</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="images/icons/favicon.png"/>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/themify/themify-icons.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="fonts/elegant-font/html-css/style.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/slick/slick.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/lightbox2/css/lightbox.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="css/util.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="css/winkelwagen.css">
    <link rel="stylesheet" type="text/css" href="css/inlog.css">
    <!--===============================================================================================-->
</head>
<div class="header1">
    <!-- Header desktop -->
    <div class="wrap_header">
        <!-- Logo -->
        <a href="index.php" class="logo">
            <img src="images/wwi.png" alt="IMG-LOGO">
        </a>

        <div class="wrap_menu">
            <nav class="menu">
                <ul class="main_menu">
                    <li class="nav-item">
                        <div class="dropdown">
                            <a class="nav-link dropbtn">Producten</a>
                            <div class="dropdown-content">
                                <?php
                                while ($row = $hresult->fetch_assoc()) {
                                    echo '<p><a href = "categorie.php?id=' . $row["StockGroupID"] . '">' . $row["stockgroupname"] . '</a></p>';
                                }
                                ?>
                            </div>
                        </div>
                    </li>

                    <li>
                        <a href="about.php">Over ons</a>
                    </li>

                    <li>
                        <a href="contact.php">Contact</a>
                    </li>
                </ul>
            </nav>
        </div>

        <div class="header-icons">
            <form method="post" action="zoeken.php">
                <div class="row rijzoeken">
                    <div class="balk">
                        <input type="text" name="zoeken" class="zoekbalk" placeholder="<?php
                        if ($_SESSION['status'] == 1) {
                            echo ("Hallo " . $_SESSION['LogonName'] . ",Wat zoek je");
                        } else {
                            echo ("Wat zoek je?");
                        }
                        ?>">
                    </div>
                    <div class="knop">
                        <input type="submit" name="search" value="zoeken" class="zoekknop">
                    </div>
                </div>
            </form>

            <span class="linedivide1"></span>

            <?php
            if ($_SESSION['status'] == 1) {
                echo "<div class='logonname'>Hallo, " . "<strong class='kleur'>" . $_SESSION['LogonName'] . "</strong></div>";
            } else {
                echo "<div class='logonname'><a class='inlogbtn' href='inlogscherm.php'>Login</a></div>";
            }
            ?>
            <a href="inlogscherm.php" class="header-wrapicon1 dis-block">
                <img src="images/icons/icon-header-01.png" class="header-icon1" alt="ICON">
            </a>

            <span class="linedivide1"></span>

            <div class="header-wrapicon2">
                <a href="Winkelwagen.php"><img src="images/icons/icon-header-02.png" class="header-icon1" alt="ICON"></a>
                <span class="header-icons-noti"><?php echo $_SESSION['waardes']; ?></span>
            </div>
        </div>
    </div>
</div>
<div class="container1-page">
    <!-- Slide1 -->




    <!-- Title Page -->
    <section class="bg-title-page1 p-t-50 p-b-900 flex-col-c-m" style="background-image: url(images/bannerwwi.png);">
        <h2 class="l-text2 t-center">
            <br>
        </h2>
        <p class="m-text13 t-center">
            <br>
        </p>
    </section>
    <body>
    <div multilinks-noscroll="true" id="content" role="main" class="clearfix"/>

<?php
$user = "root";
$pass = "";
$dbh = new PDO('mysql:host=127.0.0.1;dbname=wideworldimporters', $user, $pass);
    //Weergeven alle klantregistraties//
    $sth = $dbh->prepare("SELECT * FROM klant WHERE logonname = '" . $_SESSION['LogonName'] . "'");
    $sth->execute();
    $accountlijst = $sth->fetchAll(PDO::FETCH_BOTH);
    ?>
    <article multilinks-noscroll="true" id="content" class="clearfix">
        <h1>Account Gegevens</h1>


        <form method="POST">
            <table class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
                    <td>Gebruikersnaam</td>
                    <td>Naam</td>
                    <td>Telefoonnummer</td>
                    <td>Adres</td>
                    <td>Postcode</td>
                    <td>Woonplaats</td>
                    <td>E-mail</td>
                </tr>
                </thead>

                <?php
                //Stop alle gegevens in een tabel//
                foreach ($accountlijst as $accountdetails) {
                    print ("
	
            <tr> 
				<td>" . ($_SESSION['LogonName']) . "</td>
			    <td>" . ($accountdetails['naam']) . "</td>
				<td>" . ($accountdetails['telefoonnummer']) . "</td>
			    <td>" . ($accountdetails['adres']) . "</td>
    			<td>" . ($accountdetails['postcode']) . "</td>
				<td>" . ($accountdetails['woonplaats']) . "</td>
			    <td>" . ($accountdetails['email']) . "</td>
			</tr>	
		 ");
                }
                ?>	</table>
    </body>
</html>


<footer class="bg6 p-t-45 p-b-43 p-l-65 p-r-65 p-lr-0-xl1">
    <div class="flex-w p-b-90">
        <div class="w-size6 p-t-30 p-l-15 p-r-15 respon6">
            <h4 class="s-text12 p-b-30">
                Categorieën
            </h4>

            <ul>
                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        Novelty items
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        Clothing
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        Mugs
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        T-shirts
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        Airline Novelties
                    </a>
                </li>
            </ul>
        </div>
        <div class="flex-w p-b-90 rijcatagorie">
            <ul>
                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        Computing Novelties
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        USB Novelties
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        Furry footwear
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        Toys
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        Packaging materials
                    </a>
                </li>
            </ul>
        </div>

        <div class="w-size7 p-t-30 p-l-250 p-r-20 respon4">
            <h4 class="s-text12 p-b-30">
                Help
            </h4>

            <ul>
                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        Order traceren
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        Retourneren
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        Versturen
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        FAQs
                    </a>
                </li>
            </ul>
        </div>


    </div>
    <div class="t-center s-text8 p-t-20">
        Copyright © 2018 All rights reserved. | Deze webshop is gemaakt met <i class="fa fa-heart-o" aria-hidden="true"></i> door de ICT heroes van groep 3.
    </div>
</footer>
</div>


<!-- Back to top -->
<div class="btn-back-to-top bg0-hov" id="myBtn">
    <span class="symbol-btn-back-to-top">
        <i class="fa fa-angle-double-up" aria-hidden="true"></i>
    </span>
</div>

<!-- Container Selection1 -->
<div id="dropDownSelect1"></div>



<!--===============================================================================================-->
<script type="text/javascript" src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script type="text/javascript" src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
<script type="text/javascript" src="vendor/bootstrap/js/popper.js"></script>
<script type="text/javascript" src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script type="text/javascript" src="vendor/select2/select2.min.js"></script>
<script type="text/javascript">
    $(".selection-1").select2({
        minimumResultsForSearch: 20,
        dropdownParent: $('#dropDownSelect1')
    });
</script>
<!--===============================================================================================-->
<script type="text/javascript" src="vendor/slick/slick.min.js"></script>
<script type="text/javascript" src="js/slick-custom.js"></script>
<!--===============================================================================================-->
<script type="text/javascript" src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
<script type="text/javascript" src="vendor/lightbox2/js/lightbox.min.js"></script>
<!--===============================================================================================-->
<script type="text/javascript" src="vendor/sweetalert/sweetalert.min.js"></script>
<script type="text/javascript">
    $('.block2-btn-addcart').each(function () {
        var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
        $(this).on('click', function () {
            swal(nameProduct, "is added to cart !", "success");
        });
    });

    $('.block2-btn-addwishlist').each(function () {
        var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
        $(this).on('click', function () {
            swal(nameProduct, "is added to wishlist !", "success");
        });
    });
</script>

<!--===============================================================================================-->
<script src="js/main.js"></script>

</body>
</html>