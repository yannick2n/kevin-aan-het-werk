<html>
    <?php
//Start session
    session_start();
// The connection to the database
    include 'connect.php';
    include 'functions.php';
    $hsql = "SELECT DISTINCT sg.stockgroupname, sg.StockGroupID
            FROM StockItems s
            JOIN StockItemStockGroups g
            ON s.stockitemid = g.stockitemid
            JOIN StockGroups sg
            ON g.stockgroupid = sg.StockGroupID
            ORDER BY sg.StockGroupID";
    $hresult = $conn->query($hsql);
    if (!$hresult->num_rows > 0) {
        echo "0 results";
        return;
    }
    $conn->close();
    ?>
    <head>
        <title>WWI</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--===============================================================================================-->
        <link rel="icon" type="image/png" href="images/icons/favicon.png"/>
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="fonts/themify/themify-icons.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="fonts/elegant-font/html-css/style.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/slick/slick.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/lightbox2/css/lightbox.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="css/util.css">
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <link rel="stylesheet" type="text/css" href="css/winkelwagen.css">
        <link rel="stylesheet" type="text/css" href="css/inlog.css">
        <link rel="stylesheet" type="text/css" href="css/beheerder.css">
        <!--===============================================================================================-->
    </head>
    <div class="header1">
        <!-- Header desktop -->
        <div class="wrap_header">
            <!-- Logo -->
            <a href="index.php" class="logo">
                <img src="images/wwi.png" alt="IMG-LOGO">
            </a>

            <div class="wrap_menu">
                <nav class="menu">
                    <ul class="main_menu">
                        <li class="nav-item">
                            <div class="dropdown">
                                <a class="nav-link dropbtn">Producten</a>
                                <div class="dropdown-content">
                                    <?php
                                    while ($row = $hresult->fetch_assoc()) {
                                        echo '<p><a href = "categorie.php?id=' . $row["StockGroupID"] . '">' . $row["stockgroupname"] . '</a></p>';
                                    }
                                    ?>
                                </div>
                            </div>
                        </li>

                        <li>
                            <a href="about.php">Over ons</a>
                        </li>

                        <li>
                            <a href="contact.php">Contact</a>
                        </li>
                    </ul>
                </nav>
            </div>

            <div class="header-icons">
                <form method="post" action="zoeken.php">
                    <div class="row rijzoeken">
                        <div class="balk">
                            <input type="text" name="zoeken" class="zoekbalk" placeholder="<?php
                            if ($_SESSION['status'] == 1) {
                                echo ("Hallo " . $_SESSION['LogonName'] . ",Wat zoek je");
                            } else {
                                echo ("Wat zoek je?");
                            }
                            ?>">
                        </div>
                        <div class="knop">
                            <input type="submit" name="search" value="zoeken" class="zoekknop">
                        </div>
                    </div>
                </form>

                <span class="linedivide1"></span>

                <?php
                if ($_SESSION['status'] == 1) {
                    echo "<div class='logonname'>Hallo, " . "<strong class='kleur'>" . $_SESSION['LogonName'] . "</strong></div>";
                } else {
                    echo "<div class='logonname'><a class='inlogbtn' href='inlogscherm.php'>Login</a></div>";
                }
                ?>
                <a href="inlogscherm.php" class="header-wrapicon1 dis-block">
                    <img src="images/icons/icon-header-01.png" class="header-icon1" alt="ICON">
                </a>

                <span class="linedivide1"></span>

                <div class="header-wrapicon2">
                    <a href="Winkelwagen.php"><img src="images/icons/icon-header-02.png" class="header-icon1" alt="ICON"></a>
                    <span class="header-icons-noti"><?php echo $_SESSION['waardes']; ?></span>
                </div>
            </div>
        </div>
    </div>
    <div class="container1-page">
        <!-- Slide1 -->

        <!-- Title Page -->
        <section class="bg-title-page1 p-t-50 p-b-900 flex-col-c-m" style="background-image: url(images/bannerwwi.png);">
            <h2 class="l-text2 t-center">
                <br>
            </h2>
            <p class="m-text13 t-center">
                <br>
            </p>
        </section>
        <body>
        <br>
        <?php
        include "connect.php";
        $array = userData();
        if ($_SESSION['LogonName'] == "Beheerder") {
            if (isset($_POST['adminAction'])) {
                if (htmlspecialchars($_POST['adminAction']) == "Add item") {
                    ?>
                    <div class="additem">
                        <h1>Add a product</h1><br>
                        <form action = "verwerk.php" method = "post">
                            <label for="name">Product name: </label><br>
                            <input type="text" name="name" id="name" placeholder="Product name"><br><br>
                            <label for="category">Product Category: </label>
                            <select name="category" id="category">
                                <option value="category">Choose category...</option>
                                <?php
                                foreach (returnFromDatabase("stockgroupid", "stockgroupname", "stockgroups") as $index => $waarde) {
                                    print '<option value="' . $index . '">' . $waarde . '</option>';
                                }
                                ?>
                            </select><br>
                            <label for="supplier">Supplier: </label>
                            <select name="supplier" id="supplier">
                                <option value="supplier">Choose supplier...</option>
                                <?php
                                foreach (returnFromDatabase("supplierid", "suppliername", "suppliers") as $index => $waarde) {
                                    print '<option value="' . $index . '">' . $waarde . '</option>';
                                }
                                ?>
                            </select><br>
                            <label for="color">Color: </label>
                            <select name="color" id="color">
                                <option value="color">Choose color...</option>
                                <?php
                                foreach (returnFromDatabase("colorid", "colorname", "colors") as $index => $waarde) {
                                    print '<option value="' . $index . '">' . $waarde . '</option>';
                                }
                                ?>
                            </select><br>
                            <label for="package">Unit Packaging: </label>
                            <select name="package" id="package">
                                <option value="category">Choose packaging...</option>
                                <?php
                                foreach (returnFromDatabase("packagetypeid", "packagetypename", "packagetypes") as $index => $waarde) {
                                    print '<option value="' . $index . '">' . $waarde . '</option>';
                                }
                                ?>
                            </select><br>
                            <label for="outer">Outer Packaging: </label>
                            <select name="outer" id="outer">
                                <option value="category">Choose packaging...</option>
                                <?php
                                foreach (returnFromDatabase("packagetypeid", "packagetypename", "packagetypes") as $index => $waarde) {
                                    print '<option value="' . $index . '">' . $waarde . '</option>';
                                }
                                ?>
                            </select><br>
                            <label for="cstock">Chiller stock</label>
                            <select name="cstock" id="cstock">
                                <option value="yesno">Yes / no</option>
                                <option value="1">Yes</option>
                                <option value="0">No</option>
                            </select><br><br>
                            <label for="brand">Brand</label>
                            <input type="text" name="brand" id="brand" placeholder="Brand name"><br><br>
                            <label for="tags">Product tags: </label>
                            <input type="text" name="tags" id="tags" placeholder="32GB, USB"><br><br>
                            <label for="size">Size</label>
                            <input type="text" name="size" id="size" placeholder="S / M / L / XL etc..."><br><br>
                            <label for="timedays">Lead time days</label>
                            <input type="text" name="timedays" id="timedays" placeholder="LeadTimeDays"><br><br>
                            <label for="quantity">Quantity per outer</label>
                            <input type="text" name="quantity" id="quantity" placeholder="Quantity"><br><br>
                            <label for="bcode">Barcode</label>
                            <input type="text" name="bcode" id="bcode" placeholder="Barcode"><br><br>
                            <label for="taxrate">Taxrate</label>
                            <input type="text" name="taxrate" id="taxrate" placeholder="15.000"><br><br>
                            <label for="uprice">Unit price</label>
                            <input type="text" name="uprice" id="uprice" placeholder="69.42"><br><br>
                            <label for="marketing">Marketing comments</label>
                            <input type="text" name="marketing" id="marketing" placeholder="Kids love it!"><br><br>
                            <label for="weight">Weight</label>
                            <input type="text" name="weight" id="weight" placeholder="0.250"><br><br>
                            <input type="hidden" name="action" value="addItemToDatabase">
                            <input type = "submit" name = "submit" value = "Submit"/>
                        </form>
                        <br><br><br><br>
                    </div>
                    <?php
                } else if (htmlspecialchars($_POST['adminAction']) == "Remove item") {
                    ?>
                    <div class="removeitem">
                        <form action = "verwerk.php" method = "post">
                            <br><label for="name">Remove Product</label><br><br>
                            <input type="text" name="id" id="name" placeholder="Product id"><br>
                            <input type="hidden" name="action" value="removeItemFromDatabase">
                            <input type = "submit" name = "submit" value = "Delete"/>
                        </form>
                    </div>
                    <?php
                } else if (htmlspecialchars($_POST['adminAction']) == "Add category") {
                    ?>
                    <div class="addcategory">
                        <form action = "verwerk.php" method = "post">
                            <br><label for="name">Add Category</label><br><br>
                            <input type="text" name="name" id="name" placeholder="Category"><br>
                            <input type="hidden" name="action" value="addCategoryToDatabase">
                            <input type = "submit" name = "submit" value = "Add"/>
                        </form>
                    </div>
                    <?php
                } else if (htmlspecialchars($_POST['adminAction']) == "Edit category") {
                    ?>
                    <div class="editcategory">
                        <form action = "verwerk.php" method = "post">
                            <br><label for="name">New category name</label><br><br>
                            <br><label for="cate">Category:</label>
                            <select name="id">
                                <option value="category">Choose category...</option>
                                <?php
                                $array = returnCategories();
                                foreach (returnCategories() as $index => $waarde) {
                                    print '<option value="' . $index . '">' . $waarde . '</option>';
                                }
                                ?>
                            </select><br><br><br>

                            <input type="text" name="name" id="name" placeholder="New category name"><br>
                            <input type="hidden" name="action" value="editCategory">
                            <input type = "submit" name = "submit" value = "Edit"/>
                        </form>
                    </div>
                    <?php
                } else if (htmlspecialchars($_POST['adminAction']) == "Remove category") {
                    ?>
                    <div class="removecategory">
                        <form action = "verwerk.php" method = "post">
                            <br><label for="cate">Category:</label><br><br>
                            <center><select name="id">
                                    <option value="category">Choose category...</option>
                                    <?php
                                    $array = returnCategories();
                                    foreach (returnCategories() as $index => $waarde) {
                                        print '<option value="' . $index . '">' . $waarde . '</option>';
                                    }
                                    ?>
                                </select></center><br>
                            <br><label for="name">Action in this category:</label><br><br>
                            <center><select name="dothis">
                                    <option value="">Choose action...</option>
                                    <option value="delete">Delete all</option>
                                    <?php
                                    foreach (returnCategories() as $index => $waarde) {
                                        print '<option value="' . $index . '">Move to ' . $waarde . '</option>';
                                    }
                                    ?>
                                </select></center>
                            <input type="hidden" name="action" value="deleteCategory">
                            <input type = "submit" name = "submit" value = "Delete"/>
                        </form>
                    </div>
                    <?php
                }
            } else {

                ?>
                <div class="beheerdertitel">
                    <h1>Beheerder gedeelte</h1>
                </div>
                <div class="beheerderkeuze">
                    <form action = "#" method = "post">
                        <input class="btnbeheerder" type = "submit" name = "adminAction" value = "Voeg product toe"/>
                        <input class="btnbeheerder" type = "submit" name = "adminAction" value = "Verwijder product"/>
                        <input class="btnbeheerder" type = "submit" name = "adminAction" value = "Voeg categorie toe"/>
                        <input class="btnbeheerder" type = "submit" name = "adminAction" value = "Bewerk categorie"/>
                        <input class="btnbeheerder" type = "submit" name = "adminAction" value = "Verwijder categorie"/>
                    </form>
                </div>
                <?php
                //buttons
            }
        }
        ?>

        </body>
</html>

<footer class="bg6 p-t-45 p-b-43 p-l-65 p-r-65 p-lr-0-xl1">
    <div class="flex-w p-b-90">
        <div class="w-size6 p-t-30 p-l-15 p-r-15 respon6">
            <h4 class="s-text12 p-b-30">
                Categorieën
            </h4>

            <ul>
                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        Novelty items
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        Clothing
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        Mugs
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        T-shirts
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        Airline Novelties
                    </a>
                </li>
            </ul>
        </div>
        <div class="flex-w p-b-90 rijcatagorie">
            <ul>
                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        Computing Novelties
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        USB Novelties
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        Furry footwear
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        Toys
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        Packaging materials
                    </a>
                </li>
            </ul>
        </div>

        <div class="w-size7 p-t-30 p-l-250 p-r-20 respon4">
            <h4 class="s-text12 p-b-30">
                Help
            </h4>

            <ul>
                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        Order traceren
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        Retourneren
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        Versturen
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        FAQs
                    </a>
                </li>
            </ul>
        </div>


    </div>
    <div class="t-center s-text8 p-t-20">
        Copyright © 2018 All rights reserved. | Deze webshop is gemaakt met <i class="fa fa-heart-o" aria-hidden="true"></i> door de ICT heroes van groep 3.
    </div>
</footer>
</div>


<!-- Back to top -->
<div class="btn-back-to-top bg0-hov" id="myBtn">
    <span class="symbol-btn-back-to-top">
        <i class="fa fa-angle-double-up" aria-hidden="true"></i>
    </span>
</div>

<!-- Container Selection1 -->
<div id="dropDownSelect1"></div>



<!--===============================================================================================-->
<script type="text/javascript" src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script type="text/javascript" src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
<script type="text/javascript" src="vendor/bootstrap/js/popper.js"></script>
<script type="text/javascript" src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script type="text/javascript" src="vendor/select2/select2.min.js"></script>
<script type="text/javascript">
    $(".selection-1").select2({
        minimumResultsForSearch: 20,
        dropdownParent: $('#dropDownSelect1')
    });
</script>
<!--===============================================================================================-->
<script type="text/javascript" src="vendor/slick/slick.min.js"></script>
<script type="text/javascript" src="js/slick-custom.js"></script>
<!--===============================================================================================-->
<script type="text/javascript" src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
<script type="text/javascript" src="vendor/lightbox2/js/lightbox.min.js"></script>
<!--===============================================================================================-->
<script type="text/javascript" src="vendor/sweetalert/sweetalert.min.js"></script>
<script type="text/javascript">
    $('.block2-btn-addcart').each(function () {
        var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
        $(this).on('click', function () {
            swal(nameProduct, "is added to cart !", "success");
        });
    });

    $('.block2-btn-addwishlist').each(function () {
        var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
        $(this).on('click', function () {
            swal(nameProduct, "is added to wishlist !", "success");
        });
    });
</script>

<!--===============================================================================================-->
<script src="js/main.js"></script>

</body>
</html>