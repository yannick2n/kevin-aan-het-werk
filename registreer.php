<html>
    <?php
//Start session
    session_start();
// The connection to the database
    include 'connect.php';
    include 'functions.php';
    $hsql = "SELECT DISTINCT sg.stockgroupname, sg.StockGroupID
            FROM StockItems s
            JOIN StockItemStockGroups g
            ON s.stockitemid = g.stockitemid
            JOIN StockGroups sg
            ON g.stockgroupid = sg.StockGroupID
            ORDER BY sg.StockGroupID";
    $hresult = $conn->query($hsql);
    if (!$hresult->num_rows > 0) {
        echo "0 results";
        return;
    }
    $conn->close();
    ?>
    <head>
        <title>WWI</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--===============================================================================================-->
        <link rel="icon" type="image/png" href="images/icons/favicon.png"/>
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="fonts/themify/themify-icons.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="fonts/elegant-font/html-css/style.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/slick/slick.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="vendor/lightbox2/css/lightbox.min.css">
        <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="css/util.css">
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <link rel="stylesheet" type="text/css" href="css/winkelwagen.css">
        <link rel="stylesheet" type="text/css" href="css/inlog.css">
        <link rel="stylesheet" type="text/css" href="css/registreer.css">
        <!--===============================================================================================-->
    </head>
    <div class="header1">
        <!-- Header desktop -->
        <div class="wrap_header">
            <!-- Logo -->
            <a href="index.php" class="logo">
                <img src="images/wwi.png" alt="IMG-LOGO">
            </a>

            <div class="wrap_menu">
                <nav class="menu">
                    <ul class="main_menu">
                        <li class="nav-item">
                            <div class="dropdown">
                                <a class="nav-link dropbtn">Producten</a>
                                <div class="dropdown-content">
                                    <?php
                                    while ($row = $hresult->fetch_assoc()) {
                                        echo '<p><a href = "categorie.php?id=' . $row["StockGroupID"] . '">' . $row["stockgroupname"] . '</a></p>';
                                    }
                                    ?>
                                </div>
                            </div>
                        </li>

                        <li>
                            <a href="about.php">Over ons</a>
                        </li>

                        <li>
                            <a href="contact.php">Contact</a>
                        </li>
                    </ul>
                </nav>
            </div>

            <div class="header-icons">
                <form method="post" action="zoeken.php">
                    <div class="row rijzoeken">
                        <div class="balk">
                            <input type="text" name="zoeken" class="zoekbalk" placeholder="<?php if($_SESSION['status'] == 1){
                                echo ("Hallo ". $_SESSION['LogonName']. ",Wat zoek je");
                            } else {
                                echo ("Wat zoek je?");
                            }
                            ?>">
                        </div>
                        <div class="knop">
                            <input type="submit" name="search" value="zoeken" class="zoekknop">
                        </div>
                    </div>
                </form>

                <span class="linedivide1"></span>

                <?php
                if ($_SESSION['status'] == 1) {
                    echo "<div class='logonname'>Hallo, ". "<strong class='kleur'>". $_SESSION['LogonName']. "</strong></div>";
                } else {
                    echo "<div class='logonname'><a class='inlogbtn' href='inlogscherm.php'>Login</a></div>";
                }
                ?>
                <a href="inlogscherm.php" class="header-wrapicon1 dis-block">
                    <img src="images/icons/icon-header-01.png" class="header-icon1" alt="ICON">
                </a>

                <span class="linedivide1"></span>

                <div class="header-wrapicon2">
                    <a href="Winkelwagen.php"><img src="images/icons/icon-header-02.png" class="header-icon1" alt="ICON"></a>
                    <span class="header-icons-noti"><?php echo $_SESSION['waardes']; ?></span>
                </div>
            </div>
        </div>
    </div>
    <div class="container1-page">
        <!-- Slide1 -->

        <!-- Title Page -->
        <section class="bg-title-page1 p-t-50 p-b-900 flex-col-c-m" style="background-image: url(images/bannerwwi.png);">
            <h2 class="l-text2 t-center">
                <br>
            </h2>
            <p class="m-text13 t-center">
                <br>
            </p>
        </section>
        <body>
            <div multilinks-noscroll="true" id="content" role="main" class="clearfix"/>


            <h1 class="textregistreer"> Registratie </h1>
            <div class="tabelregisterhelemaal">
            <form class="registratie" action="registreer.php" method="POST">
                <table class="borderregister" BORDER="1" FRAME="hsides">
                    <tr>
                        <td class="tabelregister">Voornaam:</td> <td><input class="inputclass" type="text" name="voornaam" value="<?php
                            if (isset($_POST['registreer'])) {
                                echo $_POST['voornaam'];
                            }
                            ?>"/></td>
                    </tr>
                    <tr>
                        <td class="tabelregister">Tussenvoegsel:</td> <td><input class="inputclass" type="text" name="tussenvoegsel" value="<?php
                            if (isset($_POST['registreer'])) {
                                echo $_POST['tussenvoegsel'];
                            }
                            ?>" /></td>
                    </tr>
                    <tr>
                        <td class="tabelregister">Achternaam:</td> <td><input class="inputclass" type="text" name="achternaam" value="<?php
                            if (isset($_POST['registreer'])) {
                                echo $_POST['achternaam'];
                            }
                            ?>" /></td>
                    </tr>
                    <tr>
                        <td class="tabelregister">Adres*:</td> <td><input class="inputclass" type="text" name="adres" value="<?php
                            if (isset($_POST['registreer'])) {
                                echo $_POST['adres'];
                            }
                            ?>"/></td>
                    </tr>
                    <tr>
                        <td class="tabelregister">Postcode*:</td> <td><input class="inputclass" type="text" name="postcode" value="<?php
                            if (isset($_POST['registreer'])) {
                                echo $_POST['postcode'];
                            }
                            ?>" /></td>
                    </tr>
                    <tr>
                        <td class="tabelregister">Woonplaats*:</td> <td><input class="inputclass" type="text" name="woonplaats" value="<?php
                            if (isset($_POST['registreer'])) {
                                echo $_POST['woonplaats'];
                            }
                            ?>"/></td>
                    </tr>
                    <tr>
                        <td class="tabelregister">Telefoonnummer*:</td> <td><input class="inputclass" type="text" name="telefoonnummer" value="<?php
                            if (isset($_POST['registreer'])) {
                                echo $_POST['telefoonnummer'];
                            }
                            ?>"/></td>
                    </tr>
                    <tr>
                        <td class="tabelregister">Email adres*:</td> <td><input class="inputclass" type="text" name="Emailad" value="<?php
                            if (isset($_POST['registreer'])) {
                                echo $_POST['Emailad'];
                            }
                            ?>"/></td>
                    </tr>
                    <tr>
                        <td class="tabelregister">Gebruikersnaam*:</td> <td><input class="inputclass" type="text" name="LogonName" value="<?php
                            if (isset($_POST['registreer'])) {
                                echo $_POST['LogonName'];
                            }
                            ?>"/></td>
                    </tr>
                    <tr>
                        <td class="tabelregister">Wachtwoord*:</td> <td><input class="inputclass" type="password" name="wachtwoord1"/></td>
                    </tr>
                    <tr>
                        <td class="tabelregister">Wachtwoord herhalen*:</td> <td><input class="inputclass" type="password" name="wachtwoord2"/></td>
                    </tr>
                </table>
            </div>
                    <input type="submit" value="Aanmelden" name="registreer" class="btn btn-primary btn-large btnregistreer">
        </form>

            <h4 class="verplict"> Velden met * zijn verplicht </h4>

        </body>
</html>

<?php
//connectie PDO
$user = "root";
$pass = "";
$dbh = new PDO('mysql:host=127.0.0.1;dbname=wideworldimporters', $user, $pass);

//variabelen aanmaken
if (isset($_POST["registreer"])) {
    $voornaam = $_POST["voornaam"];
    $tussenvoegsel = $_POST["tussenvoegsel"];
    $achternaam = $_POST["achternaam"];
    $adres = $_POST["adres"];
    $postcode = $_POST["postcode"];
    $woonplaats = $_POST["woonplaats"];
    $telefoonnummer = $_POST["telefoonnummer"];
    $Emailad = $_POST["Emailad"];
    $LogonName = $_POST["LogonName"];
    $wachtwoord1 = $_POST["wachtwoord1"];
    $wachtwoord2 = $_POST["wachtwoord2"];
    $naam = naamplakken($voornaam, $tussenvoegsel, $achternaam);
}

// controle of alle verplichte velden zijn ingevuld
if (isset($_POST["registreer"])) {
    if (controleervelden($naam, $telefoonnummer, $adres, $postcode, $woonplaats, $Emailad, $LogonName, $wachtwoord1, $wachtwoord2) AND controleermail($Emailad) == TRUE AND controleergebruiker($LogonName) == True) {
        $wachtwoord3 = hash('sha256', $wachtwoord1);

        //query met alle ingevulde gegevens
        $sth = $dbh->prepare("INSERT INTO klant (naam, telefoonnummer, adres, postcode, woonplaats, email, LogonName, HashedPassword, IsPermittedToLogon)
            VALUES (:naam, :telefoonnummer, :adres, :postcode, :woonplaats, :Emailad, :LogonName, :wachtwoord3, 1)");

        //beveiliging
        $sth->bindValue(':naam', $naam, PDO::PARAM_STR);
        $sth->bindValue(':telefoonnummer', $telefoonnummer, PDO::PARAM_STR);
        $sth->bindValue(':adres', $adres, PDO::PARAM_STR);
        $sth->bindValue(':postcode', $postcode, PDO::PARAM_STR);
        $sth->bindValue(':woonplaats', $woonplaats, PDO::PARAM_STR);
        $sth->bindValue(':Emailad', $Emailad, PDO::PARAM_STR);
        $sth->bindValue(':LogonName', $LogonName, PDO::PARAM_STR);
        $sth->bindValue(':wachtwoord3', $wachtwoord3, PDO::PARAM_STR);

        $sth->execute();
        $aantal = $sth->rowCount();

        if ($aantal == 1) {
            echo "<br>Registratie gelukt u kunt nu inloggen.";
        } else {
            echo "<br><br>Er is een fout op getreden neemt u a.u.b contact op met de beheerder van deze website.";
        }
    }
}
?>

<footer class="bg6 p-t-45 p-b-43 p-l-65 p-r-65 p-lr-0-xl1">
    <div class="flex-w p-b-90">
        <div class="w-size6 p-t-30 p-l-15 p-r-15 respon6">
            <h4 class="s-text12 p-b-30">
                Categorieën
            </h4>

            <ul>
                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        Novelty items
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        Clothing
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        Mugs
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        T-shirts
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        Airline Novelties
                    </a>
                </li>
            </ul>
        </div>
        <div class="flex-w p-b-90 rijcatagorie">
            <ul>
                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        Computing Novelties
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        USB Novelties
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        Furry footwear
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        Toys
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        Packaging materials
                    </a>
                </li>
            </ul>
        </div>

        <div class="w-size7 p-t-30 p-l-250 p-r-20 respon4">
            <h4 class="s-text12 p-b-30">
                Help
            </h4>

            <ul>
                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        Order traceren
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        Retourneren
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        Versturen
                    </a>
                </li>

                <li class="p-b-9">
                    <a href="#" class="s-text7">
                        FAQs
                    </a>
                </li>
            </ul>
        </div>


    </div>
    <div class="t-center s-text8 p-t-20">
        Copyright © 2018 All rights reserved. | Deze webshop is gemaakt met <i class="fa fa-heart-o" aria-hidden="true"></i> door de ICT heroes van groep 3.
    </div>
</footer>
</div>


<!-- Back to top -->
<div class="btn-back-to-top bg0-hov" id="myBtn">
        <span class="symbol-btn-back-to-top">
            <i class="fa fa-angle-double-up" aria-hidden="true"></i>
        </span>
</div>

<!-- Container Selection1 -->
<div id="dropDownSelect1"></div>



<!--===============================================================================================-->
<script type="text/javascript" src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script type="text/javascript" src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
<script type="text/javascript" src="vendor/bootstrap/js/popper.js"></script>
<script type="text/javascript" src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script type="text/javascript" src="vendor/select2/select2.min.js"></script>
<script type="text/javascript">
    $(".selection-1").select2({
        minimumResultsForSearch: 20,
        dropdownParent: $('#dropDownSelect1')
    });
</script>
<!--===============================================================================================-->
<script type="text/javascript" src="vendor/slick/slick.min.js"></script>
<script type="text/javascript" src="js/slick-custom.js"></script>
<!--===============================================================================================-->
<script type="text/javascript" src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
<script type="text/javascript" src="vendor/lightbox2/js/lightbox.min.js"></script>
<!--===============================================================================================-->
<script type="text/javascript" src="vendor/sweetalert/sweetalert.min.js"></script>
<script type="text/javascript">
    $('.block2-btn-addcart').each(function () {
        var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
        $(this).on('click', function () {
            swal(nameProduct, "is added to cart !", "success");
        });
    });

    $('.block2-btn-addwishlist').each(function () {
        var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
        $(this).on('click', function () {
            swal(nameProduct, "is added to wishlist !", "success");
        });
    });
</script>

<!--===============================================================================================-->
<script src="js/main.js"></script>

</body>